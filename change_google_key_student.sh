#!/bin/bash

GOOGLE_APP_ID=$1

if [ -z "$1" ]
  then
    echo "No google app id argument supplied"
  else
    # sudo sed -i "s/$OLD_GOOGLE_ID/$GOOGLE_APP_ID/g" /www/training-center-trainee/src/helpers/tokenHelper.js
    sudo sed -i "/GID/s/\"\([^\"]*\)\"/\"$GOOGLE_APP_ID\"/g" /www/training-center-trainee/src/helpers/urlHelper.js
fi
