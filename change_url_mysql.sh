#!/bin/bash

BIND_ADDR=$1

if [ -z "$1" ]
  then
    echo "No argument supplied"
  else
    sudo sed -i "/bind-address/s/\"\([^\"]*\)\"/$BIND_ADDR/g" /etc/mysql/mysql.cnf
    systemctl status mysql.service
fi
