#!/bin/bash

OLD_ADDR="159.65.11.177"
NEW_ADDR=$1

if [ -z "$1" ]
  then
    echo "No argument supplied"
  else
    sudo sed -i "s/$OLD_ADDR/$NEW_ADDR/g" /opt/projects/trainingcenterbackend/src/main/resources/application.yml

    . /opt/properties/deploy.sh
fi