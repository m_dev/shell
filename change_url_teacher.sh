#!/bin/bash

NEW_ADDR=$1
CUSTOMER_CODE=$2

if [ -z "$1" ]
  then
    echo "No ip address argument supplied"
  else
    escaped_addr=$(printf '%s\n' "$NEW_ADDR" | sed 's:[\/&]:\\&:g;$!s/$/\\/')
    echo "Running Script... ip: $escaped_addr"
    # sudo sed -i "/development/s/\"\([^\"]*\)\"/\"$escaped_addr\"/g" /www/training-center-trainer/src/helpers/urlHelper.js
    sudo sed -i "/production/s/\"\([^\"]*\)\"/\"$escaped_addr\"/g" /www/training-center-trainer/src/helpers/urlHelper.js
fi

if [ -z "$2" ]
  then
    echo "No customer code argument supplied"
  else
    sudo sed -i "/code/s/\"\([^\"]*\)\"/\"$CUSTOMER_CODE\"/g" /www/training-center-trainer/src/helpers/tokenHelper.js
fi

cd /www/training-center-trainer

sudo yarn

sudo yarn build