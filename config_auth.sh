#!/bin/bash

OLD_CONF="PasswordAuthentication no"
NEW_CONF="PasswordAuthentication yes"

sudo sed -i "s/$OLD_CONF/$NEW_CONF/g" /etc/ssh/sshd_config

sudo systemctl reload sshd