#!/bin/bash

OLD_ADDR="167.99.69.198"
NEW_ADDR=$1

if [ -z "$1" ]
  then
    echo "No argument supplied"
  else
    # sudo sed -i "s/$OLD_ADDR/$NEW_ADDR/g" /etc/nginx/sites-enabled/default
    sudo sed -i "/^#/!s/server_name .*/server_name $NEW_ADDR/" /etc/nginx/sites-enabled/default

    sudo systemctl status nginx

    sudo systemctl restart nginx
fi

#sudo sed -i "s/$OLD_ADDR/$NEW_ADDR/g" /etc/nginx/sites-enabled/default

#sudo systemctl status nginx

#sudo systemctl restart nginx
